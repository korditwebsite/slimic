﻿<!DOCTYPE html>
<html>
  <head>
  <!-- head 33 -->

    <meta charset="UTF-8">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Hege Refsnes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" type="text/css" href="<? bloginfo('template_url') ?>/css/bootstrap.css">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="<? bloginfo('template_url') ?>/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!--WP HEAD -->
    <?php wp_head();?>
    <!--WP HEAD end -->

  </head>

  <body>
  <!--------------------- S1 ------------------------------------------------->
    <header >
      <div class=" container-fluid bg-h3">
        <div class="container" style="margin-top:38px;">
          <div class="row" style="text-align:left; ">
            <div class="col-xs-4 " >
              <div class="row">

                <div class="col-xs-6 " >
                <a href="<?php echo get_permalink('2');?>">  <img src="<? bloginfo('template_url') ?>/img/logo.png"></a>
                </div>
              </div>
            </div>
            <div class="col-xs-8">
              <div class="row">
                <!--<a class="powrot" href="<?php echo get_permalink('2');?>">  Powrót do strony głównej </a> -->


              </div>
            </div>
          </div>
        </div>
  <!--------------------- S1 ------------------------------------------------->
        <section id="s1">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 " >
                <?php
                if (have_posts()) :
                  while (have_posts()) :
                  ?><h2> <?php  the_title();  ?> </h2> <?php
                    the_post();
                     the_content();
                  endwhile;
                endif;
                 ?>
              </div>
            </div>
          </div>
        </section>
      </header>
