<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see       https://docs.woocommerce.com/document/template-structure/
 * @author    WooThemes
 * @package   WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

get_header(  ); ?>

<script>
// funkcja stylujaca zakladki (dodaje i usuwa klase "tab2style"
$(function(){

  $('input:radio[name="tabs"]').change(
    function(){
     if(this.id == "tab2"){
      var element = document.getElementById("tab2a");
      element.classList.remove("tab2style");
      element.classList.add("tab2");
    }else if(this.id == "tab3"){
      var element = document.getElementById("tab2a");
      element.classList.add("tab2style");
      element.classList.remove("tab2");
    }else{
      var element = document.getElementById("tab2a");
      element.classList.remove("tab2style");
      element.classList.add("tab2");
    }
  }
  );          

});
</script>
<!-- style CSS do sklepu-->
<link rel="stylesheet" type="text/css" href="<? bloginfo('template_url') ?>/css/single-product.css">
<?php 
// funkcja dobierajaca tlo (wraz z resonsywnoscia) w zaleznosci od produktu
$product_title = get_the_title(); 
if ( is_single(  52 )) { 
  
  echo  "<style>
            #container {background: url('http://power.projektycombo.pl/wp-content/themes/power/img/bg-produkt2.jpg') no-repeat center;background-size: cover;margin-top: -105px;padding-top: 100px;width: 100%; height:auto;text-align: left;}
  @media (max-width: 1200px){
                #container {background: url('http://power.projektycombo.pl/wp-content/themes/power/img/bg-produkt2-mobile.jpg') no-repeat center;background-size: cover;margin-top: -105px;padding-top: 100px;width: 100%;height:auto; text-align: left;}
  }
  </style>";
}else if (is_single('48')){
  
  echo  "<style>
            #container {background: url('http://power.projektycombo.pl/wp-content/themes/power/img/bg-produkt3.jpg') no-repeat center; background-size: cover;margin-top: -105px;padding-top: 100px;width: 100%; height:auto; text-align: left;}
  @media (max-width: 1200px){
                #container {background: url('http://power.projektycombo.pl/wp-content/themes/power/img/bg-produkt3-mobile.jpg') no-repeat center;background-size: cover;margin-top: -105px;padding-top: 100px;width: 100%;height:auto; text-align: left;}
  }
  </style>";
}else if (is_single('47')){

  echo  "<style>
            #container {background: url('http://power.projektycombo.pl/wp-content/themes/power/img/bg-produkt.jpg') no-repeat center;background-size: cover;margin-top: -105px;padding-top: 100px;width: 100%;height:auto; text-align: left;}
  @media (max-width: 1200px){                                                                    
                #container {background: url('http://power.projektycombo.pl/wp-content/themes/power/img/bg-produk-mobile.jpg') no-repeat center;background-size: cover;margin-top: -105px;padding-top: 100px;width: 100%;height:auto; text-align: left;}
  }
  </style>";
};?>

<?php
    /**
     * woocommerce_before_main_content hook.
     *
     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
     * @hooked woocommerce_breadcrumb - 20
     */
    do_action( 'woocommerce_before_main_content' );
    ?>

    <?php while ( have_posts() ) : the_post(); ?>

      <?php wc_get_template_part( 'content', 'single-product' ); ?>

    <?php endwhile; // end of the loop. ?>

    <?php
    /**
     * woocommerce_after_main_content hook.
     *
     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
     */
    do_action( 'woocommerce_after_main_content' );
    ?>

    <?php
    /**
     * woocommerce_sidebar hook.
     *
     * @hooked woocommerce_get_sidebar - 10
     */
    do_action( 'woocommerce_sidebar' );
    ?>
    
    <div class="container"  style="<!-- background: url(<? bloginfo('template_url') ?>/img/bg-body-mobile.jpg); background-size: cover;  --> color:#fff;">
      <div class="row">
        <div class="col-sm-12">
          <!-- zakladki u dolu strony z podpietym ACFem -->
          <!-- nawigacja -->  
          <?php if (get_field('naglowek1') && get_field('naglowek1') != "") { ?>
            <input id="tab1" type="radio" name="tabs" checked>
            <label id="tab1a"class="tab1" for="tab1"><p><?php echo get_field('naglowek1')?><span></span></p></label>
          <?php } ?>   
          <?php if (get_field('naglowek2') && get_field('naglowek2') != "") { ?>   
            <input id="tab2" type="radio" name="tabs">
            <label id="tab2a"class="tab2" for="tab2"><p><?php echo get_field('naglowek2')?></p><span></span></label>
          <?php } ?>    
          <?php if (get_field('naglowek3') && get_field('naglowek3') != "") { ?>  
            <input id="tab3" type="radio" name="tabs">
            <label id="tab3a"class="tab3" for="tab3"><p><?php echo get_field('naglowek3')?></p><span></span></label>
          <?php } ?>    
          

          <!-- zawartosc zakladek--> 
          <section id="content1">

            <div class="row" style="margin-bottom:20px;">
              <?php echo get_field('zawartosc1')?>
            </section>
            
            <section id="content2">
              <?php echo get_field('zawartosc2')?>
            </section>
            
            <section id="content3">
              <?php echo get_field('zawartosc3')?>
            </section>
            
            
          </div>
        </div>
      </div>
      
      <?php get_footer(  ); ?>
