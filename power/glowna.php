<?php /* Template Name: główna */ ?>
<?php get_header(2);?>
<!--------------------- S2 ------------------------------------------------->
<?php if( have_rows('s2') ):  while( have_rows('s2') ): the_row();?>
<section id="s2">
  <div class=" container-fluid bg2">
    <!--<div class="row">-->
    <div class="container">
      <div class="row">
        <div class="col-sm-7 col-xs-12" >
          <p class="caption">
            <?php the_sub_field('s2-title'); ?>
          </p>
          <p class="caption2">
            <?php the_sub_field('s2-desc'); ?>
          </p>
        </div>
        <div class="col-sm-1">
        </div>
        <div class="logo-s2 col-md-2 col-sm-2 col-xs-6 w100">
          <img src="<? bloginfo('template_url') ?>/img/logo-s2.png"  >
        </div>
        <div class="col-xs-12" style="margin-top:30px; "><img style="max-width:100%;" src="<? bloginfo('template_url') ?>/img/belka-info.png"></div>
        <div class="col-md-12 col-sm-12 col-xs-6 w100">
          <p class="caption3"> Nie wahaj się, zamów już dziś!<p>
            <p class="caption4"> Potwierdzona skuteczność, ekspresowe działanie!<p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-6 center w100">
              <a href="http://slimic.pl/sklep/">  <button class="button">ZAMÓW</button></a>
            </div>
            <div class="col-xs-12" style="margin-top:30px; "><img style="max-width:100%;" src="<? bloginfo('template_url') ?>/img/belka-info.png"></div>
                    <div class="col-md-12 col-sm-12 col-xs-6 w100">
          <p class="caption3"> Zobacz, co możesz zyskać dzięki SLIMIC!<p>
            <p class="caption4"> Jest idealny dla kobiet i mężczyzn!<p>
              <p class="s3-p">ZDROWY, BEZPIECZNY I PRZEDE WSZYSTKI SKUTECZNY</p>
            </div>
          </div>
        </div>
        <!--</div>-->
      </div>
    </section>
    <?php endwhile; endif; ?>
      <!--------------------- Two image ------------------------------------------------->
      <section class="two-img">
        <div class="container">
          <div class="row">
            <h3>Zdjęcia naszych klientów: </h3>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <img style="max-width:100%;" src="<? bloginfo('template_url') ?>/img/woman-img.jpg">
            </div>
            <div class="col-lg-6">
              <img style="max-width:100%;" src="<? bloginfo('template_url') ?>/img/man-img.jpg">
            </div> 
            <div class="col-lg-12 text-bg">
              <img style="max-width:100%;" src="<? bloginfo('template_url') ?>/img/text-bg.jpg">
            </div>
          </div>
        </div>
      </section>
    <!--------------------- S3 ------------------------------------------------->
    <section id="s3">
      <div class=" container-fluid bg3" id="link-body">
        <div class="row">
          <div class="container">
            <div class="row">
                            <div class="col-md-6">
                <img src="<? bloginfo('template_url') ?>/img/body-pure2.png" class="body" >
                <div class="hide_s"> <img class="logo  hide_s"src="<?php bloginfo('template_url') ?>/img/power-body2.png"></div>
              </div>
              <div class="col-md-6">     
                <div class="row" style="margin-top:60px;">
                  <?php if( have_rows('s3') ):  while( have_rows('s3') ): the_row();?>
                  <div class="col-sm-12">
                    <img src="<?php bloginfo('template_url') ?>/img/check-w.png" style="float:left; float:left; "><p><?php the_sub_field('s3-item'); ?></p>
                  </div>
                  <?php endwhile; endif; ?>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--------------------- S4 ------------------------------------------------
    <section id="s4">
      <div class=" container-fluid bg4" id="link-brain">
        <div class="row">
          <div class="container">
            <div class="row">
              <div class="col-sm-5"></div>
              <div class="col-xs-7"> <img class="logo "src="<?php bloginfo('template_url') ?>/img/power-body.png"></div>
            </div>
            <div class="row" >
              <div class="col-xs-1"></div>
              
              <div class="col-xs-2">
                <img src="<? bloginfo('template_url') ?>/img/brain-pure.png" class="body" >
                <div class="col-sm-7"> <img class="logo hide_s"src="<?php bloginfo('template_url') ?>/img/power-body.png"></div>
              </div>
              <div class="col-xs-1" style="text-align:right;">
              </div>
              <div class="col-xs-8" style="text-align:right;">
                
                <div class="row" style="margin-top:60px;">
                  <div class="col-sm-6">
                    <img src="<?php bloginfo('template_url') ?>/img/check-n.png" style="float:left; float:left; "><p>Wpływa na lepszą koncentrację</p>
                    <img src="<?php bloginfo('template_url') ?>/img/check-n.png" style="float:left; float:left; "><p>Wzmacnia układ nerwowy</p>
                    <img src="<?php bloginfo('template_url') ?>/img/check-n.png" style="float:left; float:left; "><p>Zwiększa zdolności poznwacze i intelektualne</p>
                    <img src="<?php bloginfo('template_url') ?>/img/check-n.png" style="float:left; float:left; "><p>Poprawia pamięć i spostrzegawczość</p>
                    <img src="<?php bloginfo('template_url') ?>/img/check-n.png" style="float:left; float:left; "><p>Pozytywnie wpływa na tempo i efektywność uczenia się, przyswajania informacji</p>
                  </div>
                  <div class="col-sm-6">
                    <img src="<?php bloginfo('template_url') ?>/img/check-n.png" style="float:left; float:left; "><p>Stymuluje mózg do wydajniejszej pracy i zapewnia jasność umysłu</p>
                    <img src="<?php bloginfo('template_url') ?>/img/check-n.png" style="float:left; float:left; "><p>Zapewnia większą wytrzymałość psychiczną, odporność na stres</p>
                    <img src="<?php bloginfo('template_url') ?>/img/check-n.png" style="float:left; float:left; "><p>Polecany w czasie wzmożonego wysiłku intelektualnego</p>
                    <img src="<?php bloginfo('template_url') ?>/img/check-n.png" style="float:left; float:left; "><p>Synchronizuje obie półkule mózgowe</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    ------------------- S5 ------------------------------------------------->
    <?php if( have_rows('s5') ):  while( have_rows('s5') ): the_row();?>
    <section id="s5">
      <div class=" container-fluid bg5"  >
        <div class="row">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 center">
                <img src="<? bloginfo('template_url') ?>/img/check-c.png" class="check">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 center">
                <p class="caption1"><?php the_sub_field('s5-title'); ?></p>
                <p class="caption2"><?php the_sub_field('s5-desc'); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php endwhile; endif; ?>
  <!--------------------- S6 ------------------------------------------------->
  <?php if( have_rows('s6') ):  while( have_rows('s6') ): the_row();?>
  <section id="s6">
    <div class=" container-fluid  bg6" id="link-dla-kogo">
      <div class="row" style="color:#fff; margin-top:30px;">
        <div class="container">
          <div class="row">
            <div class="col-md-1 "></div>
            <div class="col-md-2">
              <p class="caption1">Dla kogo?</p>
            </div>
            <div class="col-md-1 "></div>
            <div class="col-md-8 ">
              <p class="caption2"><?php the_sub_field('s6-desc'); ?></p>
            </div>
          </div>
          <div class="row center">
            <?php if( have_rows('s6-box') ):  while ( have_rows('s6-box') ) : the_row();  ?>
            <div class="col-sm-4 box center">
              <?php echo wp_get_attachment_image( get_sub_field('s6-item-img'), "services-box",  "",array( "class" => "img-responsive" )); ?>
              <p class="fs" style="margin:10px;"><?php the_sub_field('s6-item-desc'); ?></p>
            </div>
            <?php endwhile; else : endif; ?>
          </div>
          <div class="row center">
            <div class="col-md-12 ">
              <p class="margin"> <?php the_sub_field('s6-desc-2'); ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php endwhile; endif; ?>
  <!--------------------- S7 ------------------------------------------------->
  <section id="s7">
    <div class=" container-fluid  bg7" id="link-opinie" >
      <div class="row">
        <div class="container" style="margin-top:50px;">
          <div class="col-xs-12 s7-box"><p>Oni już z nami schudli</p><img src="<? bloginfo('template_url') ?>/img/znak.png"></div>
          <?php if( have_rows('s7') ):  while( have_rows('s7') ): the_row();?>
          <div class="row opinie ">
            <div class="col-md-2 foto">
              <?php echo wp_get_attachment_image( get_sub_field('single-opinion-img'), "services-box",  "",array( "class" => "img-responsive" )); ?>
            </div>
            <div class="col-md-10 ">
              <p class="caption"><?php the_sub_field('single-opinion'); ?></p>
            </div>
            <div class="col-sm-1"></div>
            
          </div>
          <?php endwhile; endif; ?>
        </div>
      </div>
    </div>
  </section>
  
  <!--------------------- S8 ------------------------------------------------->
</div><section id="s8">
<div class=" container-fluid bg8">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 center">
          <p class="caption1">Nie wahaj sie zamów już dziś</p>
          <p class="caption2">Potwierdzona skuteczność, ekspresowe działanie</p>
          <a href="http://slimic.pl/sklep/">  <button class="button">ZAMÓW</button></a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<?php get_footer(2);?>