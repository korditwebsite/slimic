<!DOCTYPE html>
<html  lang="pl" >
  <head>
<meta name="google-site-verification" content="ZF_dRMTa9gHHlYDX56QeeV_ajQR7sWIwdc93gajn3Dc" />  
  <!-- head 1 -->
   <meta charset="<?php bloginfo( 'charset' ); ?>" />
      
<!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" type="text/css" href="<? bloginfo('template_url') ?>/css/bootstrap.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- CSS-->

    <link rel="stylesheet" type="text/css" href="<? bloginfo('template_url') ?>/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!--WP HEAD -->
    <?php wp_head();?>
    <!--WP HEAD end -->
    <!--sklep -->
   
   
   <!--- Menu sklepu wraz z dostosowaniem wyswietlania realizacji zamowienia ----->
<?php if ( is_page(array (21, 23, 5))) { 
      echo "<style>
            #menu-koszyk > li.menu-item.menu-item-koszyk {display:none!important;}
                  
         </style>";  }
?>
<?php if ( WC()->cart->get_cart_contents_count() == 0 ) { 
         echo "<style>
            #menu-item-124 {display:none!important;}
            
         </style>";  
      } else if (is_shop() || is_product()){ 
         echo "<style>
            #menu-item-124 {display:inline-block!important;}
         </style>";
       } else { 
         echo "<style>
            #menu-item-124 {display:none!important;  /*2*/}
            
         </style>";
      }; ?>


      
   
   
  </head>

  <body>
  <!--------------------- S1 ------------------------------------------------->
    <header >
      <div class=" container-fluid "> <!-- /* bg-sklep */ -->
        <div class="container" style="margin-top:38px;">
          <div class="row" style="text-align:left; ">
            <div class="col-xs-3 " >
              <div class="row">

                <div class="col-xs-6 " >
                  <a href="<?php echo get_permalink('2');?>"> <img src="<? bloginfo('template_url') ?>/img/logo.png"> </a>
                </div>
              </div>
            </div>
               <div class="col-sm-9 col-xs-9">                
                     <nav class="nav-sklep ">
                        <?php $defaults = array(
                           'theme_location'  => 'secondary',
                           'menu'            => '',
                           'container'       => '',
                           'container_class' => '',
                           'container_id'    => '',
                           'menu_class'      => '',
                           'menu_id'         => '',
                           'echo'            => true,
                           'fallback_cb'     => 'wp_page_menu',
                           'before'          => '',  
                           'after'           => '',
                           'link_before'     => '',
                           'link_after'      => '',
                           'items_wrap'      => '<ul id="%1$s" class="%2$s klasa-sklep">%3$s</ul>',
                           'depth'           => 0,
                           'walker'          => ''
                        );
                        ?>                                  
                        <ul class="menu-sklep">
                            <?php wp_nav_menu( $defaults ); ?>

                        </ul>
                       
                     </nav>
                  </div>

              </div>
            </div>
          </div>
        </div>
  <!--------------------- S1 ------------------------------------------------->
        <section id="s1">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 " >
              </div>
            </div>
          </div>
        </section>
      </header>
