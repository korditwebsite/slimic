<?php



  add_theme_support( 'post-thumbnails' );
  the_post_thumbnail( array(500,500) );  // Other resolutions



 
function register_my_menus() {
	register_nav_menus(
		array(
			'primary' => __( 'Primary Menu' ) ,
			'secondary'=> __( 'Secondary Menu'),
		)
	);  
  }
 add_action( 'after_setup_theme', 'register_my_menus' );

 
 
//Rozmiar obrazków
add_theme_support('post-thumbnails');
add_image_size( 'logo', 220, 150 );
add_image_size( 'hero_image', 1450, 800 );
add_image_size( 'paralax-size', 1450, 1450 );
add_image_size( 'about-image', 450, 500 );
add_image_size( 'service-opacity', 360, 240 );
add_image_size( 'gallery', 360, 240, true );
add_image_size( 'about', 600, 800);


function fb_add_cart_box ( $items, $args ) {
    // only on primary menu
    if( 'secondary' === $args -> theme_location )
        $items .= '<li class="menu-item menu-item-koszyk"><a class="cart-contents" href="' . wc_get_cart_url() .'" title="'. _e( '' ) . '">' . sprintf ( _n( 'Koszyk (%d)', 'Koszyk (%d)', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ) . '&nbsp<img src="http://power.projektycombo.pl/wp-content/themes/power/img/iconcart.png" width="25" height="25" style="margin-top: -5px;"></a>
</li>';
    return $items;
}
add_filter( 'wp_nav_menu_items', 'fb_add_cart_box', 10, 2 );

		

/* WOOCOMMERCE @HOOOKS*/
//(woocommerce_after_single_product_summary

////'add_action(woocommerce_after_single_product_summary', 'woocommerce_template_single_title', 5); <-- DOAĆ POLE ACF Z LOGO
function woocommerce_template_power_single_logo(){
		echo '<img class="produkt-logo" src="' .get_field('logo'). ' ">' ;
	}

function woocommerce_template_power_single_socials(){
// ikonki social przy widoku produktow  <-------- !!!! 
		echo '<div  class="social">Podziel się: <div class="social-inner">';
		echo '<img src="'.get_bloginfo('template_url').'/img/social-fb.png" height="36" width="36" > ';
		echo '<img src="'.get_bloginfo('template_url').'/img/social-g.png" height="36" width="36" > ';
		echo '<img src="'.get_bloginfo('template_url').'/img/social-p.png" height="36" width="36" > ';
		echo '<img src="'.get_bloginfo('template_url').'/img/social-m.png" height="36" width="36" > ';
		echo '</div></div>';
}
function woocommerce_template_power_single_payments(){
		
		echo '<div class="payments">Płatności:';
		echo '<div style="float:right;"><a href="https://www.przelewy24.pl/"><img src="'.get_bloginfo('template_url').'/img/logo-p.png" height="50" width="127" ></a> </div>';
		echo '</div>';
	}
	
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
 
function woo_custom_cart_button_text() {
 
        return __( 'DODAJ DO KOSZYKA', 'woocommerce' );
 
}