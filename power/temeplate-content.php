
<?php /* Template Name: content*/ ?>
<?php
get_header(); ?>
     <section id="s1">	 
          <div class="container">
            <div class="row">
              <div class="col-sm-12 content_p" >
                <?php
                if (have_posts()) :
                  while (have_posts()) :
                  ?><h2> <?php  the_title();  ?> </h2> <?php
                    the_post();
                     the_content();
                  endwhile;
                endif;
                 ?>
              </div>
            </div>
          </div>
        </section>
<?php
get_footer();
?>
